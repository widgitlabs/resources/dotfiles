# Dotfiles

My personal dotfiles, managed with [yadm](https://yadm.io/), reworked into an
educational tool.

## Reasoning

While rebuilding both my desktop and laptop simultaneously, I realized my
personal dotfiles were woefully outdated and needed an overhaul. Widgit is a
learning community. I regularly reference this collection while helping friends
learn about Linux. I see no reason I can't bring my dotfiles into the Widgit
family and turn it into a teaching tool.

## On Dotfiles

For those not already in the know, so to speak, dotfiles are user-level
configuration files. Traditionally, this term is applied to *NIX-based
environments, due to most configuration files in those environments being named
beginning with a `.`, which identifies it as a hidden file, keeping your home
directory nice and clean.

## Installation

This isn't something you can just clone and go. This repository is tailored to
my tastes and you may (and probably will) find things which you disagree with,
or which simply won't apply in your environment. Please take the time to read
the [wiki](https://gitlab.com/widgitlabs/dotfiles/-/wikis/home) and make your
own decisions.