#!/bin/bash

# Zsh {{{

    # Disable zsh autocorrection
    unsetopt correct_all

    # Setup command completion
    autoload -Uz compinit
    compinit
    zstyle ':completion:*' menu select
    setopt COMPLETE_ALIASES

    # Setup command rehashing
    zstyle ':completion:*' rehash true

# Zsh }}}

# Oh My Zsh {{{

if [ -d "$HOME/.oh-my-zsh" ]; then
    export ZSH=$HOME/.oh-my-zsh
    export ZSH_THEME='widgit'

    if command -v code &>/dev/null; then
        export VSCODE=code
    elif command -v code-insiders &>/dev/null; then
        export VSCODE=code-insiders
    fi

    plugins=(
        catimg
        colorize
        command-not-found
        cp
        git
        sudo
        systemd
    )

    if command -v autojump &>/dev/null; then
        plugins=(autojump "${plugins[@]}")
    fi

    if command -v npm &>/dev/null; then
        plugins=(npm "${plugins[@]}")
    fi

    [ -z "$VSCODE" ] && plugins=(vscode "${plugins[@]}")

    export plugins

    # shellcheck source=/dev/null
    source "$ZSH/oh-my-zsh.sh"
fi

# Oh My Zsh }}}

# Exports {{{

export EDITOR='vim'
[ -n "$VSCODE" ] && export VISUAL="$VSCODE"
[ -z "$SSH_TTY" ] && export BROWSER=firefox || BROWSER=lynx

# Setup PATH
typeset -U PATH path
path=("${path[@]}")

if [ -d "$HOME/.bin" ]; then
    path=("$HOME/.bin" "${path[@]}")
fi

if [ -d "$HOME/Android/Sdk" ]; then
    path=("$HOME/Android/Sdk/platform-tools" "$HOME/Android/Sdk/emulator" "${path[@]}")
fi

export PATH

# Exports }}}

# Setup ssh-agent/keychain {{{

if [ -f "$HOME/.ssh/id_ed25519" ]; then
    KEYFILE='id_ed25519'
elif [ -f "$HOME/.ssh/id_rsa" ]; then
    KEYFILE='id_rsa'
fi

if [ "$KEYFILE" ]; then
    eval "$(ssh-agent)"

    if command -v keychain &>/dev/null; then
        keychain -Q -q "$KEYFILE"
        # shellcheck source=/dev/null
        [ -f "$HOME/.keychain/$HOSTNAME-sh" ] && source "$HOME/.keychain/$HOSTNAME-sh"
    fi
fi

# Setup ssh-agent/keychain }}}

# Aliases {{{

if command -v vim &>/dev/null; then
    alias vi='vim'
fi

if command -v xclip &>/dev/null; then
    alias copy='xclip -sel c <'
fi

if command -v autojump &>/dev/null; then
    alias jump='autojump'
fi

alias ..='cd ..'
alias ...='cd ../..'
alias back='cd $OLDPWD'
alias grep='grep -n --color=auto'
alias psearch='ps aux | grep'
alias ssh='ssh -A'
alias reload='source $HOME/.zshrc'

case ${OSTYPE} in
    darwin*)
    alias ls='ls -Gh'
    ;;
    *)
    alias ls='ls --group-directories-first --color=auto -h'
    ;;
esac

# Aliases }}}

# Arch specific {{{

if command -v pacman &>/dev/null; then
    alias pacman='sudo pacman'
fi

# Arch specific }}}

# Debian specific {{{

if command -v apt &>/dev/null; then
    alias apt='sudo apt'

    export UBUMAIL='Daniel J Griffiths <dgriffiths@evertiro.com>'
    export DEBFULLNAME='Daniel J Griffiths'
    export DEBEMAIL='dgriffiths@evertiro.com'
fi

# Debian specific }}}
