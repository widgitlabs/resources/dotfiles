#!/bin/bash

# Detect OS and Distro where possible {{{

# Pre-define our exports to prevent masking.
export WIDGIT_OS="Unknown"
export WIDGIT_DISTRO="Unknown"

# Check for the existence of /proc/version to allow detecting whether or not
# the current instance is running on WSL. If not, determine OS using uname. In
# WSL, uname returns the values for the guest OS rather than indicating that
# the system is running on WSL, but we need to determine the OS and distro,
# including whether or not we are using WSL, to determine how to handle various
# parts of the bootstrap process.
if [[ -f /proc/version && "$(< /proc/version)" == *@(Microsoft|WSL)* ]]; then
	WIDGIT_OS="WSL"
else
	WIDGIT_OS="$(uname -s)"
fi

# Try to get the current distro from lsb_release, assuming it exists.
# Technically, if lsb_release isn't found, we can fall back on reading values
# from /etc/*-release, but I don't have a consistent and standards compliant
# way of parsing those files (and no, directly sourcing them isn't standards
# compliant. If you know a way, please let me know!
if command -v lsb_release &>/dev/null; then
       WIDGIT_DISTRO="$(lsb_release -si)"
fi

# Detect OS and Distro where possible }}}

# Check if this is first run {{{

export WIDGIT_FIRST_RUN=true

if [ -f "$HOME/.dotfiles/installed" ]; then
	WIDGIT_FIRST_RUN=false
fi	

# Check if this is first run }}}

# Allow us to pause the script later {{{

# shellcheck disable=SC2120
pressanykey() {
	>/dev/tty printf '%s' "${*:-Press any key to continue... }"
	</dev/tty read -rsn1
	printf '\n'
}

# Allow us to pause the script later }}}

# Maybe install dependencies {{{

# Only run this on first run.
if [ "$WIDGIT_FIRST_RUN" = true ]; then

	# Handle dependencies on Debian-based systems. Currently dependency handling
	# checks for the existence of a command and installs it if necessary. This
	# isn't ideal, but it's simple. Eventually, I'll have to come up with a simple
	# way of iterating through a dataset to handle this.
	if command -v apt &>/dev/null; then
		echo "Checking for missing dependencies..."

		if ! command -v autojump &>/dev/null; then
			echo "Installing autojump..."
			sudo apt -y install autojump
		fi

		if ! command -v ag &>/dev/null; then
			echo "Installing silversearcher..."
			sudo apt -y install silversearcher-ag
		fi

		if ! command -v vim &>/dev/null; then
			echo "Installing vim..."
			sudo apt -y install vim
		fi

		if ! command -v wget &>/dev/null; then
			echo "Installing wget..."
			sudo apt -y install wget
		fi

		if ! command -v zsh &>/dev/null; then
			echo "Installing zsh..."
			sudo apt -y install zsh
		fi

		# Maybe add the Pop!_Planet Community Repository
		if [ ! -f "/etc/apt/trusted.gpg.d/pop-planet.asc" ]; then
			echo "Setting up the Pop!_Planet Community Repo..."
			sudo curl -fsSL https://apt.pop-planet.info/key.asc -o /etc/apt/trusted.gpg.d/pop-planet.asc
			sudo apt-add-repository https://apt.pop-planet.info
			sudo apt update
		fi

		# Maybe install dpkg-clean
		if ! command -v dpkg-clean &>/dev/null; then
			echo "Installing dpkg-clean..."
			sudo apt -y install dpkg-clean
		fi

		# Final cleanup, just in case
		sudo apt -y upgrade
		sudo apt -y autoremove
		sudo dpkg-clean -a
	fi
fi

# Maybe install dependencies }}}

# Maybe install Oh My ZSH {{{

if [ ! -f "$HOME/.oh-my-zsh/oh-my-zsh.sh" ]; then
	echo "Installing Oh My ZSH..."
	sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" "" --keep-zshrc
fi

# Maybe install Oh My ZSH }}}

# Setup SSH key {{{

if [ ! -f  "$HOME/.ssh/id_ed25519" ]; then
	echo "No SSH key configured, generating..."
	ssh-keygen -t ed25519 -C "$(whoami)@$(hostname)" -f "$HOME/.ssh/id_ed25519"
	echo "Use the below line to configure anything that needs your SSH key."
	cat "$HOME/.ssh/id_ed25519.pub"
	# shellcheck disable=SC2119
	pressanykey
fi

# Setup SSH key }}}

# Initialize submodules {{{

# Note that if you aren't me, this will not work. The repository defined in the
# .gitmodules file is private, but you shouldn't be installing from this repo
# directly anyway. Fork the repo, build your own private repo using the inline
# comments throughout the repo where files are included from the submodule.
# Better documentation will be put together eventually.

# Only run this on first run.
if [ "$WIDGIT_FIRST_RUN" = true ]; then

	# Ensure we are in the correct directory so that git has its working directory.
	cd "$HOME" || return

	echo "Initializing submodules..."
	yadm submodule update --recursive --init
fi

# Initialize submodules }}}

# Setup vim-plug {{{

if [ ! -f "$HOME/.vim/autoload/plug.vim" ]; then
	echo "Installing vim-plug..."
	curl -fsSLo "$HOME/.vim/autoload/plug.vim" --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
fi

# Setup vim-plug }}}

# Finished! {{{

if [ "$WIDGIT_FIRST_RUN" = true ]; then
	echo "Running first run cleanup..."
	touch "$HOME/.dotfiles/installed"

	# Because I hate clutter, the following is a quick (and really not ideal)
	# hack to prevent the README file from just always sitting in my home
	# directory. Basically, we tell yadm to ignore any local changes to the
	# file and then delete it.
	if [ -f "$HOME/README.md" ]; then
		yadm update-index --assume-unchanged "$HOME/README.md"
		rm "$HOME/README.md"
	fi
fi

# Finished! }}}
